import std.stdio;
import std.conv;
import std.path;
import std.string;
import std.array;
import std.algorithm;

const outputDir = ".";
const dubSdlPath = buildPath(outputDir, "dub.sdl");
const sourceDir = buildPath(outputDir, "source");
const appDPath = buildPath(sourceDir, "app.d");

void generateProject()
{
	import std.file : mkdirRecurse, write;

	mkdirRecurse(sourceDir);
	write(dubSdlPath, import("dub.sdl"));
}

void writeModifiedSourceFiles(int a, int b, int sum)
{
	order(a, b);
	generateProject();

	import std.file : write;

	string[] sourceLines = import("source/app.d").splitLines();
	size_t markerLocation = sourceLines.countUntil!((line, marker) => line.strip() == marker)("// MARK001");
	string newLine = format!"\telse if (a == %d && b == %d) result = %d;"(a, b, sum);
	insertInPlace(sourceLines, markerLocation, newLine);

	string source = sourceLines.join("\n");
	write(appDPath, source);
}

void main()
{
	writeln("Calculate A + B:");
	write("Enter a value for A: ");
	int a = readInt();

	write("Enter a value for B: ");
	int b = readInt();

	int sum;
	bool foundResult = operatorAdd(a, b, sum);
	if (foundResult)
		writefln!"%d + %d = %d"(a, b, sum);
	else
	{
		writeln("Did not find an answer");
		writef!"Please enter the answer for %d + %d: "(a, b);
		sum = readInt();
		writeModifiedSourceFiles(a, b, sum);
	}
}

int readInt()
{
	return readln().strip().to!int();
}

bool operatorAdd(int a, int b, out int result)
{
	order(a, b);
	if (a == 0 && b == 0) result = 0;
	else if (a == 0 && b == 1) result = 1;
	else if (a == 0 && b == 2) result = 2;
	else if (a == 0 && b == 3) result = 3;
	else if (a == 0 && b == 4) result = 4;
	else if (a == 0 && b == 5) result = 5;
	else if (a == 0 && b == 6) result = 6;
	else if (a == 0 && b == 7) result = 7;
	else if (a == 0 && b == 8) result = 8;
	else if (a == 0 && b == 9) result = 9;
	else if (a == 0 && b == 10) result = 10;
	else if (a == 1 && b == 1) result = 2;
	else if (a == 1 && b == 2) result = 3;
	else if (a == 1 && b == 3) result = 4;
	else if (a == 1 && b == 4) result = 5;
	else if (a == 1 && b == 5) result = 6;
	else if (a == 1 && b == 6) result = 7;
	else if (a == 1 && b == 8) result = 8;
	else if (a == 1 && b == 9) result = 9;
	else if (a == 1 && b == 10) result = 11;
	else if (a == 2 && b == 2) result = 4;
	else if (a == 2 && b == 3) result = 5;
	else if (a == 2 && b == 4) result = 6;
	else if (a == 2 && b == 5) result = 7;
	else if (a == 2 && b == 6) result = 8;
	else if (a == 2 && b == 7) result = 9;
	else if (a == 2 && b == 8) result = 10;
	else if (a == 2 && b == 9) result = 11;
	else if (a == 2 && b == 10) result = 12;
	else if (a == 3 && b == 3) result = 6;
	else if (a == 3 && b == 4) result = 7;
	else if (a == 3 && b == 5) result = 8;
	else if (a == 3 && b == 6) result = 9;
	else if (a == 3 && b == 7) result = 10;
	else if (a == 3 && b == 8) result = 11;
	else if (a == 3 && b == 9) result = 12;
	else if (a == 3 && b == 10) result = 13;
	else if (a == 4 && b == 4) result = 8;
	else if (a == 4 && b == 5) result = 9;
	else if (a == 4 && b == 6) result = 10;
	else if (a == 4 && b == 7) result = 11;
	else if (a == 4 && b == 8) result = 12;
	else if (a == 4 && b == 9) result = 13;
	else if (a == 4 && b == 10) result = 14;
	else if (a == 5 && b == 5) result = 10;
	else if (a == 5 && b == 6) result = 11;
	else if (a == 5 && b == 7) result = 12;
	else if (a == 5 && b == 8) result = 13;
	else if (a == 5 && b == 9) result = 14;
	else if (a == 5 && b == 10) result = 15;
	else if (a == 6 && b == 6) result = 12;
	else if (a == 6 && b == 7) result = 13;
	else if (a == 6 && b == 8) result = 14;
	else if (a == 6 && b == 9) result = 15;
	else if (a == 6 && b == 10) result = 16;
	else if (a == 7 && b == 7) result = 14;
	else if (a == 7 && b == 8) result = 15;
	else if (a == 7 && b == 9) result = 16;
	else if (a == 7 && b == 10) result = 17;
	else if (a == 8 && b == 8) result = 16;
	else if (a == 8 && b == 9) result = 17;
	else if (a == 8 && b == 10) result = 18;
	else if (a == 9 && b == 9) result = 18;
	else if (a == 9 && b == 10) result = 19;
	else if (a == 10 && b == 10) result = 10;
	// MARK001
	else return false;
	return true;
}

void order(ref int a, ref int b)
{
	if (a > b)
		swap(a, b);
}

struct Terms
{
	int a;
	int b;
}